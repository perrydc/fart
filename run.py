# useful core functions used by run.py
from flask import Flask, jsonify, render_template, redirect, url_for, request
from flask_cors import CORS # allows cross-domain requests
import datetime, sys, pathlib, traceback, os
from dotenv import load_dotenv
import markdown

# helpers for telling your server how to route requests within the app
directory = pathlib.Path(__file__).parent
sys.path.insert(1, str(directory))
app = Flask(__name__)
cors = CORS(app) # make your app cross-domain accessible
application = app #wsgi doesn't like abbreviations

# your local, custom modules
import model, model.auth, model.remoteStorage
load_dotenv(override=True) #loads environment from app root .env file, not bash.

# your custom template functions
@app.context_processor
# Model and view are distinct in Flask, but you may want to pass python functions to your templates.
# This example (used in templates/components/footer.html) can format a datetime object.
# {{ strftime(time, '%Y') }}
def strftime():
    def _strftime(time, form):
        return time.strftime(form)
    return dict(strftime=_strftime, now=model.now())

@app.context_processor
def inject_nav_vars():
    links = []
    rLinks = []
    if os.path.exists('docs'):
        docs = [f for f in os.listdir('docs') if os.path.isfile(os.path.join('docs', f))]
        for i in docs:
            filename = os.path.splitext(i)
            label = filename[0]
            link = label+filename[1]
            links.append({"label":label,"link":link})
    
    if os.path.exists('docs/user'):
        rDocs=[f for f in os.listdir('docs/user') if os.path.isfile(os.path.join('docs/user', f))]
        for i in rDocs:
            filename = os.path.splitext(i)
            label = filename[0]
            link = label+filename[1]
            rLinks.append({"label":label,"link":link})
    return dict(pubLinks=links, regLinks=rLinks)

@app.context_processor
def inject_env_vars():
    load_dotenv(override=True)
    # Careful: anything you output in jinja from these will be readable in source.
    if os.path.isfile('.env') and os.getenv('GOOGLE_CLIENT_ID'):
        environmentVariables = {
            "GOOGLE_CLIENT_ID": os.getenv('GOOGLE_CLIENT_ID'),
            "AUTHORIZED_USERS": os.getenv('AUTHORIZED_USERS')
        }
        return dict(env=environmentVariables)
    else:
        return dict(env=False)
    
# Your Routes...
@app.route('/')
# http://localhost:8000 or https://yoursite.com
def index():
    document = open("README.md", "r")
    mdText = markdown.markdown(document.read())
    return render_template('index.html', doc=mdText)

@app.route('/public/<filename>/')
def docs(filename):
    if os.path.isfile("docs/"+filename):
        documentation = open("docs/"+filename, "r")
    elif os.path.isfile("docs/unlisted/"+filename):
        documentation = open("docs/unlisted/"+filename, "r")
    if '.md' in filename:
        text = markdown.markdown(documentation.read())
    else:
        text = documentation.read()
    return render_template('index.html', doc=text)
    
@app.route('/private/<filename>/')
def privateDocs(filename):
    gToken = request.cookies.get("gToken")
    gProfile = model.auth.token2profile(gToken)
    if os.path.isfile("docs/private/"+filename):
        documentation = open("docs/private/"+filename, "r")
    elif os.path.isfile("docs/unlisted/private/"+filename):
        documentation = open("docs/unlisted/private/"+filename, "r")
    mdText = markdown.markdown(documentation.read())
    if 'error' in gProfile:
        mdText = 'Error: '+gProfile['error']
    elif gProfile['email'] not in model.auth.AUTHORIZED_USERS:
        mdText = 'Error: '+gProfile['email']+' is not authorized to view this page.'
    return render_template('index.html', doc=mdText)

@app.route('/user/<mdFilename>/')
def userDocs(mdFilename):
    gToken = request.cookies.get("gToken")
    gProfile = model.auth.token2profile(gToken)
    documentation = open("docs/user/"+mdFilename, "r")
    mdText = markdown.markdown(documentation.read())
    if 'error' in gProfile:
        mdText = 'Error: '+gProfile['error']
    elif gProfile['email'] not in model.auth.AUTHORIZED_USERS:
        mdText = 'Error: '+gProfile['email']+' is not authorized to view this page.'
    return render_template('index.html', doc=mdText)

@app.route('/api/<functionName>', methods=['GET', 'POST'])
# an api that returns the timestamp plus any post or get data you send in the request.
# For a more direct connection between the endpoint and a database, see:
#   https://flask-restful.readthedocs.io/en/latest/quickstart.html#a-minimal-api
# or, if you're looking for something that can serve distributed clients at scale, see:
#   https://serverless.com/blog/flask-python-rest-api-serverless-lambda-dynamodb/
def api(functionName):
    # test like model.exampleFunction({"post": "frog"}, {"get": "toad"})
    postData = {}
    if request.method == 'POST':
        postData = request.json # this grabs the postData from the request
    getData = request.args # or, of you prefer to set params in the url, you can pull all as a dict.
    # finally, you can specify the arguments you're looking for like getData['varName']
    # a simple response would be: return jsonify(model.exampleFunction(postData, getData))
    # but we're applying it to all functions in the file model.__init__.py
    return jsonify(globals()['model'].__dict__[functionName](postData, getData))

@app.route('/papi/<functionName>', methods=['GET', 'POST'])
# similar to the above route, but only works with a valid token issued by google.
def privateAPI(functionName):
    postData = {}
    if request.method == 'POST':
        postData = request.json
    getData = request.args
    if postData and 'token' in postData:
        profile = model.auth.token2profile(postData['token'])
    else:
        return jsonify({"error":"No token in postData."})
    if profile and 'email' in profile and profile['email'] in model.auth.AUTHORIZED_USERS:
        return jsonify(globals()['model'].auth.__dict__[functionName](postData, getData))
    else:
        return jsonify({"error":"User is not authorized."})
        
@app.route('/remoteStorage/<dbAction>', methods=['GET', 'POST'])
# http://localhost:8000/remoteStorage/setItem (with post data in error handling example below)
def userDB(dbAction):
    postData = request.get_json
    if postData and 'token' in postData:
        profile = model.auth.token2profile(postData['token'])
        if 'email' in profile:
            email = profile['email']
        else:
            return jsonify({"error":"invalid token"})
    else:
        return jsonify({"error":"No token in postData."})
    if dbAction == 'setItem' and 'key' in postData and 'value' in postData:
        return jsonify(model.remoteStorage.setItem(email, postData['key'], postData['value']))
    elif dbAction == 'getItem' and 'key' in postData:
        return jsonify(model.remoteStorage.getItem(email, postData['key']))
    elif dbAction == 'removeItem' and 'key' in postData:
        return jsonify(model.remoteStorage.removeItem(email, postData['key']))
    elif dbAction == 'getItems':
        return jsonify(model.remoteStorage.getItems(email))
    else:
        return jsonify({
            "error":"Poorly formed query.",
            "I'm looking for /remoteStorage/<getItems|setItem|removeItem|getItem>":{
                "token": "UrT0k3nFr0mTh3g00GL3s",
                "key": "favorite_color",
                "value": "Blue"
            },
            "But instead you gave me": postData
        })

# error-handling
@app.errorhandler(Exception)
# this fallback route drastically simplifies debugging in Flask by showing you where your code broke.
def exception_handler(error):
  tb = traceback.format_exc().split('\n')
  return jsonify(error=repr(error), line=tb[-3], location=tb[-4], traceback=tb)

# need an ssl cert? see: https://woile.github.io/posts/local-https-development-in-python-with-mkcert/
# Then, in terminal, brew install nss and brew install mkcert.  Then: 
# mkcert -cert-file cert.pem -key-file key.pem 0.0.0.0 localhost 127.0.0.1 ::1
# for local development (the if __name__ part) only.  This is ignored on live.
if __name__ == "__main__":
    #app.run() # default would run on 127.0.0.1:5000, but Google doesn't allow IP as an auth domain
    app.run(host="localhost", port=8000, debug=True) # G allows http in a local env only. Need https?
    #app.run(ssl_context=('cert.pem', 'key.pem'), host="localhost", port=8000, debug=True)
