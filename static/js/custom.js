function ajaxRequest(endpoint, postData = {}, successCallback, ...optParams) {
    var http = new XMLHttpRequest();
    http.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            r = JSON.parse(this.response)
            if (r.error) {
                console.log('error: ' + r.error);
            } else {
                console.log('Response: ' + JSON.stringify(r));
            }
            successCallback(r, optParams)
        }
    }
    http.open('POST', endpoint, true);
    http.setRequestHeader('Content-Type', 'application/json');
    http.send(JSON.stringify(postData));
}

function putResponseInDiv(r, optParams) {
    divID = optParams[0]
    document.getElementById(divID).innerHTML = JSON.stringify(r, null, "\t");
}

function updateLocalStorage(r) {
    localStorage.setItem("storedUserData",r);
}

function constructPrivateMenu(r) {
    pLinks = r;
    menuHTML = '';
    for (let i = 0; i < pLinks.length; i++) {
        menuHTML += "<li><a href='/private/"+pLinks[i]['link']+"'/>"+pLinks[i]['label']+"</a></li>";
    }
    console.log('authorized user sees additional links: '+menuHTML);
    document.getElementById("authorizedLinks").innerHTML = menuHTML;
    localStorage.setItem("menuHTML", menuHTML)
}

window.addEventListener("load",function(event){
    if ( document.getElementById("authorizedLinks") ){
        if (localStorage.getItem("menuHTML") === null) {
            ajaxRequest("/papi/authorizedLinks",
                        {"token":localStorage.getItem("gToken")
                        },constructPrivateMenu)
        } else {
            console.log('Got auth menu from localStorage.');
            document.getElementById("authorizedLinks").innerHTML = localStorage.getItem("menuHTML");
        }
    }
},false);

// examples: ajaxRequest(endpoint, {"token":localStorage.getItem("gToken")}, putResponseInDiv, divID)

// remoteStorage.getItems:
// var postData = {"token":localStorage.getItem("gToken")}
// ajaxRequest('/remoteStorage/getItems', postData, updateLocalStorage)

// remoteStorage.setItem:
// var postData = {"token":localStorage.getItem("gToken"),"key":setItemKey,"value":setItemValue}
// ajaxRequest('/remoteStorage/setItem', postData, updateLocalStorage)

// remoteStorage.removeItem:
// var postData = {"token":localStorage.getItem("gToken"),"key":setItemKey}
// ajaxRequest('/removeItem/setItem', postData, updateLocalStorage)