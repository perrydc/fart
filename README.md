# What is the fart?

Fart is a Fast App Rendering Tool built on [Tailwind](https://tailwindcss.com/).  Need a new microservice with a Google authentication?  Squeeze it out in record time.  And writing simple HTML and documentation for your new Flask-Python app is alimentary with the built-in markdown transformer.  But first, step into the reading room and peruse the documentation below.  It's not just a lot of hot air.

## Install the fart
1. Toot on over to gitlab and claim it proudly:

        git clone git@gitlab.com:perrydc/fart.git your_project
        cd your_project
        git remote remove origin

2. Select **Create blank project** on the [gitlab New project page](https://gitlab.com/projects/new)
3. Give your project a name, untick **Initialize repo...** and click **create project**
4. Click **Code** and copy the path under **Clone with SSH**
5. Back in Terminal, paste what you just copied after **git remote add origin** and cut one loose.

        git remote add origin {git@gitlab.com:your_name/your_project.git}
        git branch -M main
        git push -uf origin main

## Run your fart locally
It's *your* fart now.  Let 'er rip!

    python run.py;

If you see "Serving Flask app..." it's working!  Poop that IP path into your browser and breathe in the sweet tang of success.

#### Oh God!  What did you just do?!
If your Terminal emits a **ModuleNotFoundError**, install that module and try again.

    pip install {name_of_missing_module}

## Need more?
Full documentation on busting your fart on a production server, cutting an API, or lighting up Google One-Tap Authentication are included in the hamburger menu of your local fart.  So go ahead, install it already!