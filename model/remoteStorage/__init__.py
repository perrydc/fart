# /remoteStorage/setItem?myCat=Tom -- w/postData token preprocessed to email by router
from peewee import *
import os, datetime, json

dbPath = 'model/remoteStorage/data.db'
db = SqliteDatabase(dbPath)

class BaseModel(Model):
    class Meta:
        database = db
    
class User(BaseModel):
    email = CharField(unique=True) # email like: 'erooney@glenbrook.edu'
    items = CharField(default='{}') # json string like: '{"favorite color":"blue"}'
    updated = DateTimeField(default=datetime.datetime.now()) # datetime object
    
if not os.path.exists(dbPath):
    db.connect()
    db.create_tables([User])
    
def setItem(email, key, value):
    user, created = User.get_or_create(email=email)
    item = json.loads(user.items)
    item[key] = value
    User.update(items=json.dumps(item)).execute()
    return item

def getItems(email):
    user = User.select().where(User.email == email)
    if user.exists():
        return json.loads(user[0].items)
    return {"error:": "user does not exist."}
        
def removeItem(email, key):
    user = User.select().where(User.email == email)
    if user.exists():
        item = json.loads(user[0].items)
        if key in item:
            item.pop(key)
            User.update(items=json.dumps(item)).execute()
            return item
    return {"error:": "user or item does not exist."}
            
def getItem(email, key):
    user = User.select().where(User.email == email)
    if user.exists():
        item = json.loads(user[0].items)
        if key in item:
            return item[key]
    return {"error:": "could not find item."}