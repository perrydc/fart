# pip install --upgrade google-api-python-client
from google.oauth2 import id_token
from google.auth.transport import requests
import os
from dotenv import load_dotenv
load_dotenv(override=True) #loads environment from app root .env file, not bash.
AUTHORIZED_USERS = os.getenv('AUTHORIZED_USERS')

def token2profile(token):
    CLIENT_ID = os.getenv('GOOGLE_CLIENT_ID')
    try:
        # Valid gToken, get profile
        idinfo = id_token.verify_oauth2_token(token, requests.Request(), CLIENT_ID)
        return idinfo
    except ValueError:
        # invalid token, return error
        return {
            "error": "Can't validate token."
        }

def exampleAuthenticatedFunction(postData={}, getData={}):
    # you don't need this if you delete Private APIs from the menu.
    profile = token2profile(postData['token'])
    output = profile
    return output
    
def authorizedLinks(postData={}, getData={}):
    # keep this one as it populates the menu for authorized-users.
    links = []
    pDocs=[f for f in os.listdir('docs/private') if os.path.isfile(os.path.join('docs/private', f))]
    for i in pDocs:
        filename = os.path.splitext(i)
        label = filename[0]
        link = label+filename[1]
        links.append({"label":label,"link":link})
    return links