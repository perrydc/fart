# What The Fart is Tailwind?
Tailwind can be effortlessly customized, so all your sites don't look alike.  Also unlike Bootstrap, Tailwind isn't built on a satanic library like jQuery.  In fact, it doesn't have any javascript at all, so you get to write your own!

I jest.  Tailwind *does* play nice with vanilla javascript, but there are dozens of Tailwind-ready libraries out there.  I'm using [flowbite](https://flowbite.com/docs/getting-started/quickstart/) to make the drop-down menu work, but since the library is already installed, you can use it for almost any interaction you can dream up.  Flowbite is also a funny name.  Not as funny as Beefcushion, but it'll do.

Bootstrap may be the barking spider of CSS libraries, but there's nothing funny about that name.  So we won't use it here.

Similar to Bootstrap, you can drop Tailwind classes into any element, and they're a bit more intuitively named.  But here, I thought it would be nice to apply base classes to all elements using the @apply decorator.  That allows us to leverage the compactness of a CSS library, while keeping everything in one place (in this case, at the top of **/templates/components/head.html**).

## Get serious: the customization checklist
To remove all fart references and match the site to your brand, do the following:

1. In **templates/components/nav.html**, swap your logo svg for the python link next to fart (and change fart to something else).

        <!-- Logo and site name -->
        <a id="bug" href="/">
            <img class="h-8" alt="Python Logo" src="https://s3.dualstack.us-east-2.amazonaws.com/pythondotorg-assets/media/files/python-logo-only.svg"/>
            <span>fart</span>
        </a>

2. In **templates/components/footer.html**, put your org name in place of You and add the links to your privacy policy, etc.

        <span> © {{ strftime(now, '%Y') }}, You. All Rights Reserved. </span>

3. In **templates/components/head.html**, swap in your favicon and tweak the CSS to your specifications.

        <!-- favicon -->
        <link rel="icon" href="{{ url_for('static', filename='img/favicon.ico') }}" sizes="64x64" />
        
## My static JS / CSS won't update.  What gives?
Browsers cache anything in the /static folder.  When you make a change to one of these files, go into **templates/base.html** and update the version number at the top:

    {% set version = '0.0.0' %}
    
The version number will be appended to the file reference in the page source and that will notify your browser to re-fetch the data.

### So why is the static/css/custom.css empty
For the reason above (I figure most folks will forget to increment the version number).  But also because Tailwind is so efficient that the full styling doesn't take up much space.