# Deploy your fart
When it's time to share your stink-monster with the world, you'll need to designate its containing folder as a python application.  If you're guffing in a folder on an existing domain, you can skip over to **Fart-ops**.

## Bust your fart on a subdomain (optional)
If you really want to spice things up, put your app on a **subdomain**!  On most virtual hosts, the process is:

1. Go to **cPanel -> Domains** to add the subdomain.  
2. Toggle your CDN to development mode and add the A record for that subdomain.
3. Set up an https cert in **cPanel -> Let's Encrypt SSL**
4. Toggle development mode back off on your CDN

(Enter that https domain (not the folder) as the App Domain/URI in step 5 below)

## Fart-ops
1. From the SSH prompt in your production server projects folder, clone the repo:

        git clone git@gitlab.com:yourGitID/your_project.git

2. Create a backup of your run.py file since it will get overwritten by cPanel in the next step

        cd your_project
        mv run.py runBU.py

3. Open 'Setup Python App' from cPanel and select Create Application
4. Select Python version > 3.0
5. Select App directory (example: python/your_project)
6. Select App Domain/URI (example: your_project.yoursite.com)
7. Set startup file as **run.py** and entry point as **application**
8. optional: set log file location, like **logs/your_project**
9. optional: set environment variables (secrets you don't want in your repo)
![Setup Python App example screenshot](https://media.sourcewolf.com/img/setup.jpg)
10. Review the screenshot above for syntax and click **CREATE** in the upper right corner
11. You should now see a link to "enter to virtual environment." Copy that command to your clipboard and enter it as an alias in your **server's .bash_profile**.  It'll look something like **alias apps='source ~/virtualenv/python/your_project/3.7/bin/activate && cd ~/python/your_project'**.  While you're at it, these two (totally optional) lines will also come in handy, if 
they're not already on your server:

        alias pull='git pull; touch tmp/restart.txt'
        alias c='env EDITOR=nano crontab -e'

12. Once you save, remember to source your bashprofile so these aliases will be available immediately:

        source ~/.bashrc

13. When you hit CREATE a few steps back, you overwrote your run.py file, so you'll see **It works!** if you navigate to the domain (which is the overwritten cPanel code).  
14. To reinstate your code, just copy over that backup file you created a few steps back and rebase:

        mv runBU.py run.py
        git config pull.ff only

15. Install dependencies.  The best way to do this is to activate the virtual host in shell using the handy **your_project** alias you just created.  From there, start installing dependencies, occassionally checking to see what's missing by typing **python run.py**.  At the very least, you'll probably need:

        pip install flask flask_cors python-dateutil requests peewee python-dotenv markdown
        pip install --upgrade google-api-python-client

16. After you've run **python run.py** a few times from your application folder, you'll stop seeing dependency errors and you'll see an error like **Address already in use** when you type **python run.py**.  You're almost there!  Use that **pull** alias you set up earlier to **restart the app**.  It should work now when you navigate to the address, and if it doesn't you'll see what's wrong with your code if you followed the optional step above.
17. (recommended) For error handling, navigate to the domain root folder (something like ~/apps.yourdomain.com) and add the following near the top (on the second line) of your .htaccess file (this isn't in the repo because the public site you set up in cPanel sits outside the repo):

        PassengerFriendlyErrorPages on

  
18. For security, you should also force https on your domain (assuming you have followed the above instructions to create an ssl cert for your domain) by adding the following lines to the bottom of your .htaccess file:

        RewriteEngine On 
        RewriteCond %{HTTPS} off 
        RewriteRule ^(.*)$ https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]

## Troubleshooting: Why the stink face?
If you got a 500 INTERNAL SERVER ERROR at that url, use your server shell to navigate to the project root on your server and type:

    python run.py
  
Python is pretty good about explaining errors.  You probably just need to update your dependencies (see **[Run your fart locally](/#:~:text=If your Terminal emits a ModuleNotFoundError, install that module and try again)**).