<h1>Private API Reference</h1>

<p>By placing this explainer page in the docs/private folder, it is restricted by default.  Only your authorized users will see the page in the menu.  If an anonymous or unauthorized user <em>does</em> find the page, the content will not load.</p>

<p>Here we show both cookie-based authentication (the way this explainer page is handled) and post data based authentication.  The codeblock below displays the current, sign-in user's raw profile info from Google.  Note that this is coming directly from a token that was just validated on the back-end, so it is secure.</p>

<pre class="mb-0" id="pre1">Oh Nos! Javascript borken.</pre>
<script>
ajaxRequest(
    "/papi/exampleAuthenticatedFunction", 
    {"token":localStorage.getItem("gToken")}, 
    putResponseInDiv, "pre1"
)
</script>

<sub>If you see { "error": "Can't validate token." } above, sign in and hit refresh.  Also compare your <a href="https://console.cloud.google.com/apis/credentials">credential settings</a> with the <a href="/public/Authenticate.md/">set-up instructions</a>.</sub>

<h2>How do it know?</h2>

<p>We're sending a generic ajax call to <a href="/papi/exampleAuthenticatedFunction">/papi/exampleAuthenticatedFunction</a>.  The "papi" stands for Private API and will help us to distinguish between public and private functions.  The Google token (stored in localStorage by default) should be present in all your private requests:</p>

<pre>
&lt;pre class=&quot;mb-0&quot; id=&quot;pre1&quot;&gt;Oh Nos! Javascript borken.&lt;/pre&gt;
&lt;script&gt;
ajaxRequest(
    &quot;/papi/exampleAuthenticatedFunction&quot;,
    {&quot;token&quot;:localStorage.getItem(&quot;gToken&quot;)},
    putResponseInDiv, &quot;pre1&quot;
    )
&lt;/script&gt;
</pre>

<p>Similarly, every function you write in the model/auth folder should require a token, like so:</p>

<pre>def examplePrivateFunction(postData={}, getData={}):
    profile = token2profile(postData['token'])
    if 'error' in profile:
        error = profile
        return error
    else:
        output = profile
        return output</pre>

<p>Keep in mind that the above example will work for any <em>authenticated</em> user.  If we want to restrict the function to only respond to <em>authorized</em> users, we'd need to specify that with additional code.  Something like:</p>

<pre>
if postData and 'token' in postData:
    profile = token2profile(postData['token'])
else:
    return {"error":"No token in postData."}
</pre>

That's how the beginning of the authorizedLinks function works.  The endpoint is called at the beginning of an authorized session and stored in localStorage for the remainder of the session.

<h2>How is Google Validating the user?</h2>

<p>When you authenticate, the browser asks Google to display a login button on the site.  When you click it, Google compares the domain with the site-owner's client id to make sure it's valid (and if isn't, it sends back a blank popup or a popup with an error message).</p><p>
    
If it <em>is</em> valid, Google passes a JWT token back to the site javascript.  The JWT token is a 1200 character base-64 hash that, when decoded, contains the user's profile info, including name, email address and avatar.  The token is only 'valid' for about an hour, but the profile info can be read from the token even after it expires.
</p><p>
We store the token in localStorage for as long as it lives, and then we ask the user to log in again when it expires.  During its life, we can pass the token back to the server and <strong>the server will independently validate the token with Google</strong> which prevents a hacker from claiming to be someone else and stealing data or performing an unauthorized transaction.  Google notes that you should never pass a user ID to the back-end, and this is exactly why: valid tokens can be bandied about freely (even to other origin servers!) without losing the critical function of authentication: to show that the user really is who he says he is.
</p>

<h2>What if I want to extend sign-in past an hour?</h2>

<p>Don't do that!</p>

<p>Seriously: the dirty secret of authentication is that every time you force people to log in, you are making the implicit claim that your application will keep their data safe.  If there is a breech, you could be liable not only for the data used to operate your application, but any data that might assist a hacker in doing harm to your users elsewhere.  So far, all authentication in this app (now <em>your</em> app) is being handled by one of the 10 largest companies in the world.  Google wrote the python library that validates every user request on the back-end: let your users sue them.  All you need to do is periodically:</p>

<pre>pip install --upgrade google-api-python-client</pre>

<h3>Whatever, dude, I want to be signed in forever!</h3>

<p>
Fine.  The old-school way to do this would be to store the user data in a lightweight ORM (I recommend peewee + SQLite) and issue a cookie back to the user.  Every time they make a secure request, you use their cookie to look up their data.  You'll want to set the ttl (time to live) of the cookie and store the expiration date on the server.  You'll also have to trigger some cleanup script that periodically deletes expired cookies from the table.  I roughed out the <a href="https://gitlab.com/perrydc/fart/-/blob/dad57c6ac41c032e1be5796cc1a415ea7e93ef82/templates/components/auth/google_button.html">front-end</a> and <a href="https://gitlab.com/perrydc/fart/-/blob/dad57c6ac41c032e1be5796cc1a415ea7e93ef82/model/auth/__init__.py">back-end</a> of a cookie-based solution in an <a href="https://gitlab.com/perrydc/fart/-/commit/dad57c6ac41c032e1be5796cc1a415ea7e93ef82">earlier iteration</a> of this project, but I abandoned that approach as needlessly complicated and really fucking insecure.
</p>

<h2>A more modern method</h2>

<p>Google issued the original token.  Why not issue your own?  You can add one step to sign-in that encodes whatever data you'll need in future in a token and sends it back to the browser to store in place of the original token.  You're still adding a step of home-grown authentication (and the responsibility to keep your encryption key secret), but at least you're not storing any sensitive user data.</p>

<h3>To issue and decode your own token:</h3>

<p>Install pyjwt in Terminal:</p>

<pre>
pip install pyjwt
</pre>

<p>Then, to initialize and encode your first token, use:</p>

<pre>
import jwt
encoded_jwt = jwt.encode({"email": "youremail@gmail.com"}, "secret", algorithm="HS256")
</pre>

<p>Finally, read back the email address to use as an identifier in future:</p>

<pre>
jwt.decode(encoded_jwt, "secret", algorithms=["HS256"])['email']
</pre>

<p>
Note that if you really use "secret" as your secret, a hacker could easily use the same code above to infiltrate your server.  Make sure you come up with a secret that only you know and store it in your .env file to reduce the risk of someone finding it.</p>
<p>
Again, <b>I recommend you just leave the code as-is</b>.  There's something elegant (dare I say <em>beautiful?</em>) about modern authenication: no cookies, no database, no cron jobs to enforce your ttl on the server.  The typical user isn't going to spend more than an hour in a session anyway, and Google One Click makes it easy to authenicate with each session.</p>

<p>Short-term authentication lets your users know that, when they visit, you'd appreciate a hello.</p>