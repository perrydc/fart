<h1>Public API Reference</h1>

<p>With Flask, you would normally have to define your routes, file paths, inputs and outputs to build a simple API endpoint like this one.  You'd also have to fuss with AJAX to make the javascript call.  Here I've organized the file system to cut out all of those steps.</p>

<p>Let's start with the output below.  Go ahead, hit refresh to see the timestamp increment and the random message update.  I could watch this all day.</p>

<pre class="mb-0" id="pre1">Oh Nos! Javascript borken.</pre>
<script>ajaxRequest("/api/exampleFunction?fart=excuses", {}, putResponseInDiv, "pre1")</script>

<sub>You can reach this public API endpoint by going to <a href="/api/exampleFunction?fart=excuses">/api/exampleFunction?fart=excuses</a>.  Feel free to change the key-value pair.</sub>

<h2>What are the steps?</h2>

<p>I opened <strong>model/__init__.py</strong> and dropped a simple function:</p>

<pre>
def exampleFunction(postData={}, getData={}):
    fartonym = ["Air biscuit","Back draft", ... ]
    fartAction = ["Answer the call of the wild burrito" ... ]
    message = ("Please excuse my "+random.choice(fartonym).lower()
        + ". I had to "+random.choice(fartAction).lower()+".")
    return {
        'message': message,
        'timestamp': now().isoformat(),
        'postData': postData,
        'getData': getData
    }
</pre>

<p>That will create an endpoint at /api/exampleFunction.  The two things that are important here are the optional postData and getData parameters, which allow us to ignore routes entirely.  It's also important that the output is a dictionary.</p>

<p>As an optional second step, I also created this documentation page "Public APIs.html" and dropped it in the docs/ folder.  That puts this documentation in the nav, and also allows us to display the output using a our generic ajax request:</p>

<pre>
&lt;pre class=&quot;mb-0&quot; id=&quot;pre1&quot;&gt;Oh Nos! Javascript borken.&lt;/pre&gt;
&lt;script&gt;ajaxRequest(&quot;/api/exampleFunction?fart=excuses&quot;, {}, putResponseInDiv, &quot;pre1&quot;)&lt;/script&gt;
</pre>

<p>If you copy this short bit of code and swap out your own function name as the endpoint, you'll have API documentation in no time.</p>
