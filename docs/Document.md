# Documenting your code
Fart is designed to promote microservices, so mostly you're going to build unstyled embeds and APIs, like [this example api](/api/exampleFunction?fart=excuses).  But I've built in a routing loop and adopted the hilariously named Tailwind to make documentation easy.

## How do I add a new page?
Any file you save in that folder will automatically be added to the main menu.  For simplicity, I've added a markdown processor for all files saved with the suffix .md.  First time using Markdown?  Take a look at the example in the /doc folder (like this page, for instance), or check out the [basic syntax documentation](https://www.markdownguide.org/basic-syntax/).  

Markdown looks like this (note formatting for Headline, SubHead and link):

    # Documenting your code
    Fart is designed to promote microservices, so mostly you're going to build ...

    ## How do I add a new page?
    First time using [markdown](https://www.markdownguide.org/getting-started/)? ...
    
To show a responsive image, you can drop an image into the /static/img folder, and reference like this:

    ![alt-text](/static/img/filename.jpg)
    
Note that images take up space, so you might want to exclude these from your repo by adding the /static/img directory to .gitignore.  That will necessitate loading the images to your production environment separately.  

A better way is to use another server for your images.  The wordpress media library, for instance, works well.  Then use the absolute url to reference the image in markdown.

## What if I want to add html?
Markdown is great, but it can be limiting.  If you're frequently limited, you can [extend markdown](https://python-markdown.github.io/extensions/) to handle things like tables and syntax highlighting.

Or you can add HTML directly to any markdown document.  Note that  [Jinja](https://jinja.palletsprojects.com/en/3.1.x/templates/#synopsis) within an .md file will be stripped out.  You can create jinja templates in the root templates folder, but you'll need to also build a custom route in the run.py file and manually add a link to it from the templates/nav component if you want it to be in the menu.

**Note:** HTML can confuse the markdown processor, particularly when you're using tabs to make your code easier to follow.  So you should only include simple HTML within markdown documents.  For more complex HTML, toggle markdown off entirely by saving the file as .html.  All other rules (menu placement, positioning within the document structure, etc, will still apply.

## Can I include icons?
<svg class="w-6 h-6 mr-2 inline" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="w-6 h-6">
  <path d="M7.493 18.5c-.425 0-.82-.236-.975-.632A7.48 7.48 0 0 1 6 15.125c0-1.75.599-3.358 1.602-4.634.151-.192.373-.309.6-.397.473-.183.89-.514 1.212-.924a9.042 9.042 0 0 1 2.861-2.4c.723-.384 1.35-.956 1.653-1.715a4.498 4.498 0 0 0 .322-1.672V2.75A.75.75 0 0 1 15 2a2.25 2.25 0 0 1 2.25 2.25c0 1.152-.26 2.243-.723 3.218-.266.558.107 1.282.725 1.282h3.126c1.026 0 1.945.694 2.054 1.715.045.422.068.85.068 1.285a11.95 11.95 0 0 1-2.649 7.521c-.388.482-.987.729-1.605.729H14.23c-.483 0-.964-.078-1.423-.23l-3.114-1.04a4.501 4.501 0 0 0-1.423-.23h-.777ZM2.331 10.727a11.969 11.969 0 0 0-.831 4.398 12 12 0 0 0 .52 3.507C2.28 19.482 3.105 20 3.994 20H4.9c.445 0 .72-.498.523-.898a8.963 8.963 0 0 1-.924-3.977c0-1.708.476-3.305 1.302-4.666.245-.403-.028-.959-.5-.959H4.25c-.832 0-1.612.453-1.918 1.227Z" />
</svg> Yes, though you'll need to style them.  The steps are:

1. Go to [heroicons.com](https://heroicons.com/)
2. Hover over the icon you want and click 'Copy JSX'
3. Paste that code into your document and add size/position classes inside the svg tag:

        <svg class="fill-current w-6 h-6 mr-2 inline" ...
        
## Why not just include a CMS?

It would be fairly easy to build a CMS on top of this that would allow more collaborative editing.  You could add an upload image function with a simple GUI that is only accessible to authorized users.  You could also tap into the existing file structure to edit or add new documents, like so:
<hr class="mt-10">
<div class="mt-6 grid gap-3 grid-cols-3"><div>
<label for="folders">Folders:</label>
<select id="folders" name="public">
<option value="docs/">Public Menu</option>
<option value="docs/private/">Private Menu</option>
<option value="docs/user/">User Menu</option>
<option value="docs/unlisted/">Public Unlisted</option>
<option value="docs/unlisted/private">Public Unlisted</option>
</select>
</div><div>
<label for="public">Edit Existing:</label>
<select id="public" name="public">
<option value="Authenticate.md">Authenticate</option>
<option value="Customize.md">Customize</option>
<option value="Deploy.md">Deploy</option>
<option value="Document.md">Document</option>
</select>
</div><div>
<button id="editArticle" onclick="editArticle()" class="mt-9 p-2 text-base">
Edit
</button>
<button id="newArticle" onclick="newArticle()" class="mt-9 p-2 text-base ml-10">
Add New
</button>
<button id="deleteArticle" onclick="deleteArticle()" class="mt-9 p-2 text-base">
Delete
</button>
</div>
</div>
<div class="mb-10">
<label for="message" class="block mb-2 mt-8 font-medium text-gray-900 dark:text-white">Document Markdown</label>
<textarea id="message" rows="4" class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="# Documenting your code
Fart is designed to promote microservices, so mostly you're going to build unstyled embeds and APIs, like [this example api](/api/exampleFunction?fart=excuses).  But I've built in a routing loop and adopted the hilariously named Tailwind to make documentation easy."></textarea>
</div>
<hr class="mb-10">

You could even create a button that moves documents from folder to folder.  This would allow you to collaborate with others to build out the site without forcing them to update through git.

So why didn't I do that?

Because I think it defeats the purpose of Flask, which is to build out lightweight microservices with python making up the majority of the logic.  It also complicates the build process.  To avoid conflicts, you'd have to put your whole document folder in .gitignore and create a cron job on the server that periodically backs up that folder.

If you want a CMS, use wordpress.  If you want to build out an API that can run complex calculations, use this.  You can always hand your output off to wordpress.