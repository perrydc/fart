# User Database

I've included a user database with a simple API in the package.  First, take a look at the data assigned to you via the [remoteStorage/getItems](remoteStorage/getItems) endpoint.  You have no data.  Sad.  Read on below the controls to see how you can start to interact with the database in a secure way.

<pre class="mb-0" id="pre1">Javascript borken.  Pull my finger.</pre>

<div class="mt-4 grid gap-3 mb-8 md:grid-cols-2">
    <div>
        <label for="setItemKey" class="text-sm">Key:</label>
        <input id="setItemKey" onclick="this.value=''" value="favorite smell"></input>
    </div>
    <div>
        <label for="setItemValue" class="text-sm">Value:</label>
        <input id="setItemValue" onclick="this.value=''" value="fabreeze"></input>
    </div>
    <div>
        <button id="removeItemButton" onclick="removeItemExample()">
            Remove Item
        </button>
    </div>
    <div>
        <button id="setItemButton" onclick="setItemExample()">
            Set Item
        </button>
    </div>
</div>

<script>
    var postData = {"token":localStorage.getItem("gToken")}
    ajaxRequest('/remoteStorage/getItems', postData, putResponseInDiv, "pre1")
    
    function setItemExample() {
        setItemKey = document.getElementById('setItemKey').value;
        setItemValue = document.getElementById('setItemValue').value;
        var postData = {"token":localStorage.getItem("gToken"),"key":setItemKey,"value":setItemValue}
        ajaxRequest('/remoteStorage/setItem', postData, putResponseInDiv, "pre1")
    }
    function removeItemExample() {
        itemKey = document.getElementById('setItemKey').value;
        var postData = {"token":localStorage.getItem("gToken"),"key":itemKey}
        ajaxRequest('/remoteStorage/removeItem', postData, putResponseInDiv, "pre1")
    }
</script>
<p>What you're looking at is a dictionary assigned to your email address which can only be edited with a validated JWT token from Google (you might want to read the article on <a href = "/user/Private%20APIs.md/">Private APIs</a> if you haven't already).  Because it's just a dictionary, you can assign values to it.  Go ahead: Click the Set Item button on the right hand side and watch what happens.</p>
<p>Now click Remove Item.  Cool, right?</p>
<p>Did I mention you can add <em>any</em> value you want?  Now try something wild: Put your own key / value pair in the input boxes and click Set Item.</p>

<h2>How do I use these in my code?</h2>
<p>Remember localStorage.setItem() from the auth example?  We're going to do something like that with <em>remote</em> storage.</p>
<ol>
    <li>Get Items</li>
    <li>Set Item</li>
    <li>Remove Item</li>
</ol>
<p>Currently, these three items are just fancy calls to our generic ajax function:</p>
<pre>
// remoteStorage.getItems:
var postData = {"token":localStorage.getItem("gToken")}
ajaxRequest('/remoteStorage/getItems', postData, updateLocalStorage)

// remoteStorage.setItem:
var postData = {"token":localStorage.getItem("gToken"),"key":setItemKey,"value":setItemValue}
ajaxRequest('/remoteStorage/setItem', postData, updateLocalStorage)

// remoteStorage.removeItem:
var postData = {"token":localStorage.getItem("gToken"),"key":setItemKey}
ajaxRequest('/removeItem/setItem', postData, updateLocalStorage)
</pre>

<p>But pretty soon I'm going to figure out how to encapsulate these as:</p>
<ol>
    <li>remoteStorage.getItems()</li>
    <li>remoteStorage.setItem("key", "value")</li>
    <li>remoteStorage.removeItem("key")</li>
</ol>
<p>But wait, why not remoteStorage.getItem("key")?  Because if we set those three up correctly — keeping the server in sync with the user dictionary in the browser's local storage — we can pull individual keys from localStorage.</p>