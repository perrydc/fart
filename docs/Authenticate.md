# Authentication and Authorization
If your application needs restricted access, you'll need some form of authentication.  With the advent of CCPA and GDPR, it is advisable to track users without storing data.  This can be easily achieved with the use of cryptographic tokens that are issued by a server and stored on the browser.  This starter project piggie backs on Google's validation platform, so you'll need to set up a client ID with Google.  The good news is that once you drop your client id in the .env file, authentication will just. work.

Before you begin, note that this auth code requires the google python library, which you'll need to install on both your local machine and (eventually) on your production server. From your root environment:

    pip install --upgrade google-api-python-client

## Enable sign-in
To enable Google sign-in, simply create a .env file with your client ID and secret:

1. Create authorization credentials with Google by clicking the **Configure Application** button on the [Google sign-in documentation](https://developers.google.com/identity/oauth2/web/guides/get-google-api-clientid).  On initial set-up, use the exact domain / subdomain of your production environment.  You'll add localhost javascript origins in a future step to make this work locally.
2. At the last step, keep the screen open with your new client_id.
3. From the root folder in Terminal, type:

        cp sample.env .env

4. Edit your .env file with your key:

        nano .env

5. Copy your client ID from the end of step 2 into the fields in the .env file and add your email address to the list of Authorized Users.
6. Head over to the [Google dev console](https://console.cloud.google.com/apis/dashboard), select your project from the dropdown, click **Credentials**, and click **OAuth Client**.
7. Under **Authorized JavaScript origins** add **http://localhost** and **http://localhost:8000**
8. Under **Authorized redirect URIs** add **http://localhost:8000/redirect.html**
9. While you're there, you might also want to add the js origin domain and redirect URI for your production server.  It'll be something like **https://sub.yourdomain.com** for the js origin and **https://sub.yourdomain.com/redirect.html** for the redirect
10. (optionally) [Style the button](https://developers.google.com/identity/gsi/web/reference/js-reference#GsiButtonConfiguration).  For instance, if you thought it would look better as a square icon, you would add type: "icon" under google.accounts.id.renderButton in templates/auth/google_button.html.

Once you've performed the above steps, sign in to your application with your own Google account.  You can read more about how this works and learn about setting up restricted access pages and private APIs in the new menu item, "Private APIs" which only appears to signed-in users.

## Other third-party sign-ins may require a local ssl cert
Google allows http from localhost, but other libraries (such as facebook) require **https**.  View the bottom of run.py for instructions on installing an ssl cert on your local environment.  Working https run code is already included (but commented out, since Google doesn't need it).